//
//  DetailHeaderView.swift
//  Challenge2
//
//  Created by Piera Marchesini on 19/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import UIKit

class DetailHeaderView: UIView {

    @IBOutlet weak var imageView: UIImageView!

}
