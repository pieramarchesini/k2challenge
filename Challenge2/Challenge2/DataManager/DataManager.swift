//
//  DataManager.swift
//  Challenge2
//
//  Created by Piera Marchesini on 18/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import UIKit

final class DataManager {
    static let sharedInstance = DataManager()
    private let apiKey = "ae723360-2b09-11e8-9baf-d9754e3828a4"
    var artPieces = [ArtPiece.Record]()
    
    fileprivate init() { }
    
    func getArtPieces(completed: @escaping () -> ()) {
        let jsonUrlString = "https://api.harvardartmuseums.org/object?apikey="+self.apiKey+"&size=300"
        
        guard let url = URL(string: jsonUrlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else { return }
            do {
                let arts = try JSONDecoder().decode(ArtPiece.self, from: data)
                DispatchQueue.main.sync {
                    self.artPieces = arts.records
                    completed()
                }
            } catch let jsonErr {
                print("Error serializing json:", jsonErr)
            }
            }.resume()
        
    }
    
}
