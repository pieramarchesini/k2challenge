//
//  TitleDetailTableViewCell.swift
//  Challenge2
//
//  Created by Piera Marchesini on 19/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import UIKit

class TitleDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleDetailLabelOutlet: UILabel!
    
    var detailedArt: ArtPiece.Record? {
        didSet {
            if let people = detailedArt?.people?.first {
                self.titleDetailLabelOutlet.text = people.name
            } else {
                self.titleDetailLabelOutlet.text = "Not specified"
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
