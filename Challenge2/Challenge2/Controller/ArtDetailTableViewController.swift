//
//  ArtDetailTableViewController.swift
//  Challenge2
//
//  Created by Piera Marchesini on 19/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import UIKit

class ArtDetailTableViewController: UITableViewController {
    
    var detailArt: ArtPiece.Record?
    var headerView: DetailHeaderView!
    var headerMaskLayer: CAShapeLayer!
    
    private let tableHeaderViewHeight: CGFloat = 350.0
    private let tableHeaderViewCutaway: CGFloat = 40.0

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        self.navigationItem.largeTitleDisplayMode = .never
        
        //Set up the stretchy header effect
        if let headerView = tableView.tableHeaderView as? DetailHeaderView {
            self.headerView = headerView
            if let image = detailArt?.images?.first {
                headerView.imageView.downloadedFrom(link: image.baseimageurl)
            } else {
                headerView.imageView.image = #imageLiteral(resourceName: "notSpecified")
            }
            tableView.tableHeaderView = nil
            tableView.addSubview(headerView)
            
            tableView.contentInset = UIEdgeInsets(top: tableHeaderViewHeight, left: 0, bottom: 0, right: 0)
            self.tableView.contentOffset = CGPoint(x: 0, y: -tableHeaderViewHeight + 64)
            //Cut away header view
            self.headerMaskLayer = CAShapeLayer()
            self.headerMaskLayer.fillColor = UIColor.black.cgColor
            self.headerView.layer.mask = self.headerMaskLayer
            
            let effectiveHeight = tableHeaderViewHeight - tableHeaderViewCutaway/2.5
            tableView.contentInset = UIEdgeInsets(top: effectiveHeight, left: 0, bottom: 0, right: 0)
            tableView.contentOffset = CGPoint(x: 0, y: -effectiveHeight)
            
            updateHeaderView()
        }
    }
    
    // MARK: - Stretchy header effect
    func updateHeaderView() {
        let effectiveHeight = tableHeaderViewHeight - tableHeaderViewCutaway/2.5
        var headerRect = CGRect(x: 0, y: -effectiveHeight, width: tableView.bounds.width, height: tableHeaderViewHeight)
        if tableView.contentOffset.y < -effectiveHeight {
            headerRect.origin.y = tableView.contentOffset.y
            headerRect.size.height = -tableView.contentOffset.y + tableHeaderViewCutaway/2.5
        }
        self.headerView.frame = headerRect
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: 0))
        path.addLine(to: CGPoint(x: headerRect.width, y: headerRect.height))
        path.addLine(to: CGPoint(x: 0, y: headerRect.height-self.tableHeaderViewCutaway))
        
        self.headerMaskLayer?.path = path.cgPath
    }
    
    // MARK: - ScrollView method
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateHeaderView()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let art = self.detailArt else { return UITableViewCell() }
        if indexPath.row == 0 {
            guard let titleCell = tableView.dequeueReusableCell(withIdentifier: "titleDetailCell", for: indexPath) as? TitleDetailTableViewCell else { return UITableViewCell() }
            titleCell.detailedArt = art
            return titleCell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "infoDetailCell", for: indexPath) as? InfoDetailTableViewCell else { return UITableViewCell() }
            switch indexPath.row {
            case 1:
                cell.nameInfoLabelOutlet.text = "Year"
                if let year = self.detailArt?.accessionyear {
                    cell.descriptionInfoLabelOutlet.text = "\(year)"
                } else {
                    cell.descriptionInfoLabelOutlet.text = "Year not specified"
                }
            case 2:
                cell.nameInfoLabelOutlet.text = "Technique"
                if let technique = self.detailArt?.technique {
                    cell.descriptionInfoLabelOutlet.text = technique
                } else {
                    cell.descriptionInfoLabelOutlet.text = "Technique not specified"
                }
            case 3:
                cell.nameInfoLabelOutlet.text = "Culture"
                if let culture = self.detailArt?.culture {
                    cell.descriptionInfoLabelOutlet.text = culture
                } else {
                    cell.descriptionInfoLabelOutlet.text = "Culture not specified"
                }
            case 4:
                cell.nameInfoLabelOutlet.text = "Classification"
                if let classification = self.detailArt?.classification {
                    cell.descriptionInfoLabelOutlet.text = classification
                } else {
                    cell.descriptionInfoLabelOutlet.text = "Classification not specified"
                }
            default:
                break
            }
            return cell
        }
    }

}
