//
//  ArtPieceTableViewCell.swift
//  Challenge2
//
//  Created by Piera Marchesini on 18/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import UIKit

class ArtPieceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var namePeopleLabelOutlet: UILabel!
    @IBOutlet weak var yearLabelOutlet: UILabel!
    @IBOutlet weak var cultureLabelOutlet: UILabel!
    @IBOutlet weak var techniqueLabelOutlet: UILabel!
    @IBOutlet weak var classificationLabelOutlet: UILabel!
    @IBOutlet weak var artImageOutlet: UIImageView!
    let standardString = "Not especified"
    
    var artPiece: ArtPiece.Record?{
        didSet {
            if let name = self.artPiece?.people?.first {
                self.namePeopleLabelOutlet.text = name.name
            } else {
                self.namePeopleLabelOutlet.text = self.standardString
            }
            if let year = self.artPiece?.accessionyear {
                self.yearLabelOutlet.text = "\(year)"
            } else {
                self.yearLabelOutlet.text = self.standardString
            }
            if let technique = self.artPiece?.technique {
                self.techniqueLabelOutlet.text = technique
            } else {
                self.techniqueLabelOutlet.text = self.standardString
            }
            if let culture = self.artPiece?.culture {
                self.cultureLabelOutlet.text = culture
            } else {
                self.cultureLabelOutlet.text = self.standardString
            }
            if let classification = self.artPiece?.classification {
                self.classificationLabelOutlet.text = classification
            } else {
                self.classificationLabelOutlet.text = self.standardString
            }
            if let imageArray = self.artPiece?.images {
                var found = false
                for image in imageArray {
                    if image.displayorder == 1 {
                        self.artImageOutlet.downloadedFrom(link: image.baseimageurl)
                        found = true
                        break
                    }
                }
                if (!found) {
                    self.artImageOutlet.image = #imageLiteral(resourceName: "notSpecified")
                }
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
