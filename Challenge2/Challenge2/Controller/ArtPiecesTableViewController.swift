//
//  ArtPiecesTableViewController.swift
//  Challenge2
//
//  Created by Piera Marchesini on 18/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import UIKit

class ArtPiecesTableViewController: UITableViewController {
    var dataManager = DataManager.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        dataManager.getArtPieces {
            self.tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataManager.artPieces.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "artPieceCell", for: indexPath) as? ArtPieceTableViewCell else { return UITableViewCell() }
        cell.artPiece = self.dataManager.artPieces[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toArtDetail", sender: self)
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ArtDetailTableViewController, let row = self.tableView.indexPathForSelectedRow?.row {
            destination.detailArt = dataManager.artPieces[row]
        }
    }

}
