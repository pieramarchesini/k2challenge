//
//  ArtPiece.swift
//  Challenge2
//
//  Created by Piera Marchesini on 18/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import Foundation

struct ArtPiece: Decodable {
    let records: [Record]
    
    struct Record: Decodable {
        let accessionyear: Int?
        let technique: String?
        let culture: String?
        let classification: String?
        let people: [People]?
        let images: [Image]?
    }
    
    struct People: Decodable {
        let name: String
    }
    
    struct Image: Decodable {
        let displayorder: Int
        let baseimageurl: String
    }
}
