//
//  LongestCommonPrefixViewController.swift
//  Challenge1
//
//  Created by Piera Marchesini on 18/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import UIKit

class Challenge1ViewController: UIViewController {

    @IBOutlet weak var solutionLabelOutlet: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nameFile = "strings"
        let textFromFile = File.readFile(from: nameFile)
        let solution = LongestCommomPrefix(text: File.storeInArray(text: textFromFile))
//        let solution = LongestCommomPrefix(text: ["geeks", "gee", "geets"])
        self.solutionLabelOutlet.text = solution.longestCommonPrefix()
    }

}
