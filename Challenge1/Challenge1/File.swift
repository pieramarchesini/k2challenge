//
//  File.swift
//  Challenge1
//
//  Created by Piera Marchesini on 18/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import Foundation

class File {
    
    // MARK: - Read and store string from file
    class func readFile(from fileName: String) -> String {
        if let path = Bundle.main.path(forResource: fileName, ofType: ".txt") {
            let url = URL(fileURLWithPath: path)
            do {
                let text = try String(contentsOf: url, encoding: .utf8)
                return text
            }
            catch {
                /* error handling here */
            }
        }
        return ""
    }
    
    class func storeInArray(text: String) -> [String] {
        var array = [String]()
        text.enumerateLines { (line, _) in
            array.append(line)
        }
        return array
    }
}
