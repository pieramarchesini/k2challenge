//
//  LongestPrefix.swift
//  Challenge1
//
//  Created by Piera Marchesini on 18/03/18.
//  Copyright © 2018 Piera Marchesini. All rights reserved.
//

import Foundation

class LongestCommomPrefix {
    var strings: [String]
    
    init(text: [String]) {
        self.strings = text
    }
    
    func longestCommonPrefix() -> String {
        var result = ""
        var shortestWord = strings[0]
        
        if self.strings.count == 0 {
            return "There is no words in the array"
        }
        
        strings.forEach { (string) in
            if shortestWord.count > string.count {
                shortestWord = string
            }
        }
        
        for ch in shortestWord {
            let newPrefix = result + String(ch)
            let allContainPreffix = strings.reduce(0) {
                $1.hasPrefix(newPrefix) ? $0 + 1 : $0
                } == strings.count
            if allContainPreffix {
                result = newPrefix
            } else {
                break
            }
        }
        if result == "" {
            return "Could not find the longest common prefix string"
        }
        return result
    }
}
